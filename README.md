# README #

This README would normally document whatever steps are necessary to get your application up and running.

Virtual Keyboard application has been compiled on a Windows 10 machine running Visual Studio 17. Please ensure that a windows machine is being
used as the application makes use of few system level functionalities for GUI.


# Features #

1.) Console shows an on screen virtual keyboard (Font: BLUE COLOR)
2.) Receives input from user (Font: Red)
3.) Prints input at the bottom of the console window sequentially. 
4.) The characters entered by the input blink on the onScreenKeyboard sequentially for a fixed amout of time.
5.) Evalueates for string concatenation or integer addition operation internally and returns output.
	5.1) In case for string concatenation expression must start with "=" followed by string and "+". For eg =Ab+C+DE returns AbCDE.
	5.2) In case for integer concatenation expression must start with "=" followed by integer and "+". Expressions with spaces aren't evaluated. For eg =1+2 returns 3 but =1 +2 return the same string.

6.) Input "ctrl q" exits the application.



Please see to it that a detailed class diagram added to the repository by the name of uml_ClassDiagram under the repository.