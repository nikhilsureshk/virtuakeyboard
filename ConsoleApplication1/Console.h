#pragma once
#include <iostream>
#include <windows.h>
#include <vector>
#include "GameConstants.h"
#include <assert.h> 
#include <string>
#include <unordered_map>
#include <map>
#include "InputString.h"

namespace VirtualKeyboard
{
	class Console
	{
	public:
		Console(const Console& rhs) = default;
		~Console();	
		static Console* GetInstance();
		void CreateKeys();		
		void ProcessInput();
		template <typename T>
		void PrintOutput(T data,int xCoordinate, int yCoordinate)
		{
			SetCursorPosition(xCoordinate, yCoordinate);
			SetConsoleTextColor();
			std::cout << data;
		}
		template <typename T>
		void Blink(T data, int xAxixCoord, int yAxicCoord)
		{
			SetConsoleTextColor();
			for (int i = 0; i <= 5; i++)
			{
				SetCursorPosition(xAxixCoord, yAxicCoord);
				std::cout << " ";
				Sleep(100);
				SetCursorPosition(xAxixCoord, yAxicCoord);
				std::cout << data;
				Sleep(100);
			}
		}
		void SetCursorPosition(int kXAxis = CursorPositions::InputXAxis, int kYAxis = CursorPositions::InputYAxis);
		void SetConsoleTextColor(int kColorOffset = SystemColors::Blue);
		bool GetConsoleRunning() const { return m_bConsoleRunning; }
	private:	
		Console(); //Private Default Constructor
		void InitializeConsole();
		void Animate(const std::string &inp);
		void SetFontsize(int a, int b);
		
		void DefineKeyboard();
	
		static Console* _ConsoleInstance;
		HANDLE _Console;
		HWND _Hwnd;
		std::map< char, int> keyboardIndexMap;
		COORD root = { CursorPositions::InputXAxis, CursorPositions::InputYAxis };
		unsigned int m_OutputXAxisCoords;
		unsigned int m_OutputYAxisCoords;

		bool m_bConsoleRunning;
		
	};

	 

	
}


