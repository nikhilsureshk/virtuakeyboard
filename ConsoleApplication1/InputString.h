#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include "GameConstants.h"

namespace VirtualKeyboard
{
	class InputString
	{
	public:
		InputString() = delete;
		InputString(const std::string &inpString);
		~InputString() = default;
		std::string GetResult() ;
	private:
		void ProcessString();
		bool isValidExpression();

		std::vector<char> m_vOperators;

		std::string m_InputString;
		int  m_CharacterExpression;
	};
}
