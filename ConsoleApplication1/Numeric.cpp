#include "Numeric.h"
using namespace VirtualKeyboard;

Numeric::Numeric(const int &ref, const int& xAxisPos, const int& yAxisPos) :
	m_uIntData(ref)
{
	SetKeyType(CharType::Int);
	SetCursorPosition(xAxisPos, yAxisPos);
}

void Numeric::SetKeyType(const int keyType)
{
	m_uKeyType = keyType;
}

int Numeric::GetKey() const
{
	return m_uIntData; 
}