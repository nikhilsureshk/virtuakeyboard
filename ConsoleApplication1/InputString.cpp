#include "InputString.h"

namespace VirtualKeyboard
{
	InputString::InputString(const std::string &inpString) :
		m_InputString(inpString),
		m_CharacterExpression(0)
	{
	}

	void InputString::ProcessString()
	{
		if (isValidExpression())
		{
			for (int i = 0; i < m_InputString.size(); i++)
			{

				if (ispunct(m_InputString[i]) && m_InputString[i] == '+')
				{
					m_vOperators.push_back(m_InputString[i]);
				}
			}
			
			std::vector<std::string> vec;
			for (int i = 0; i < m_vOperators.size(); i++)
			{

				std::string delim;
				delim += m_vOperators[i];
				size_t pos = 0, prevPos = 0;
				bool m_bSplit = true;
				while (m_bSplit) {
					pos = m_InputString.find(delim, prevPos);
					if (pos == std::string::npos) {

						m_bSplit = false;
					}
					vec.push_back(m_InputString.substr(prevPos, pos - prevPos));
					prevPos = pos + delim.length();
				}
				m_vOperators.erase(m_vOperators.begin());
			}
			if (vec.size())
			{
				m_InputString = "";
				if (m_CharacterExpression)
					for (int i = 0; i < vec.size(); i++)
					{
						m_InputString += vec[i];
					}

				else
				{
					int result = 0;
					for (int i = 0; i < vec.size(); i++)
					{
						result += stoi(vec[i]);
					}
					m_InputString = std::to_string(result);
				}
			}						
		}	
}


	std::string InputString::GetResult()
	{
		ProcessString();
		return m_InputString;

	}

	bool InputString::isValidExpression()
	{	
		std::string simplifiedString = m_InputString;
;
		if (simplifiedString[0] == '=')
			simplifiedString.erase(0, 1);
		else return false;
		

		if (std::any_of(simplifiedString.begin(), simplifiedString.end(), [](char op) {return op == '+'; }) && std::any_of(simplifiedString.begin(), simplifiedString.end(), ::isalpha))
		{
			m_CharacterExpression = 1;
			m_InputString = simplifiedString;
			return true;
		}
		else
		{
			int index = 0;				
			while (index < simplifiedString.size())
			{			
				if (ispunct(simplifiedString[index]) && simplifiedString[index] == AllowedOperator)
				{			
					if ((!index) || index == simplifiedString.size() - 1)
					{
						return false;
					}		
					index++;
					
				}				
				else if (isalnum(simplifiedString[index]))
				{
					index++;
				}
				else 
					return false;						
			}			
		}
		m_InputString = simplifiedString;
		return true;
		

	}
}
