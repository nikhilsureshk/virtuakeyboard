#ifndef CHARACTER
#define CHARACTER

#include "Literal.h"

namespace VirtualKeyboard
{
	class Character : public Literal
	{
	public:
		Character() = delete;
		Character(const char &ref, const int& xAxisPos, const int& yAxisPos);
		virtual void SetKeyType(const int keyType);
		virtual char GetKey() const;
	private:
		char m_wChar;
		
	};
}

#endif //CHARACTER