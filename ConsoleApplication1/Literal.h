#ifndef LITERAL
#define LITERAL

#include <iostream>
#include <windows.h>
#include "GameConstants.h"
#include "Console.h"

namespace VirtualKeyboard
{
	class Literal
	{
	public:
		Literal() = default;
		~Literal() = default;
		virtual void SetCursorPosition(const int kXAxis, const int kYAxis) 
		{
			m_uXAxis = kXAxis;
			m_uYAxis = kYAxis;
		}
		int GetKeyType() { return m_uKeyType; }
		int GetXAxis() { return m_uXAxis; }
		int GetYAxis() { return m_uYAxis; }
	protected:
		virtual void SetKeyType(const int keyType) = 0 ;
		unsigned int m_uKeyType;	
		unsigned int m_uXAxis;
		unsigned int m_uYAxis;
	};
}

#endif // LITERAL



