#include "Character.h"

using namespace VirtualKeyboard;

Character::Character(const char &ref, const int& xAxisPos, const int& yAxisPos):
	m_wChar(ref)
{
	SetKeyType(CharType::Char);
	SetCursorPosition(xAxisPos, yAxisPos);
}

void Character::SetKeyType(const int keyType)
{
	m_uKeyType = keyType;
}

char Character::GetKey() const
{
	return m_wChar;	
}