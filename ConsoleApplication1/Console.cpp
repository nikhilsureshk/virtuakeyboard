#include "Console.h"
#include "Literal.h"
#include "Keys.h"
#include "Numeric.h"
#include "Character.h"
#include <memory>

using namespace std;
using namespace VirtualKeyboard;


Console* Console::_ConsoleInstance;
static Keys* m_pKey;
static vector<std::shared_ptr<Literal>> KeyCollection;


Console::Console()
{
	InitializeConsole();
}

Console::~Console()
{
	delete _ConsoleInstance; 
	_ConsoleInstance = NULL;
}

Console* Console::GetInstance()
{	
	if (!_ConsoleInstance)
		_ConsoleInstance = new Console();
	return _ConsoleInstance;
}

void Console::InitializeConsole()
{
	m_bConsoleRunning = true;
	_Console = GetStdHandle(STD_OUTPUT_HANDLE);		
	m_pKey = NULL;
	m_OutputXAxisCoords = CursorPositions::OutputXAxis;
	m_OutputYAxisCoords = CursorPositions::OutputYAxis;
	_Hwnd = GetConsoleWindow();
	SetFontsize(30, 30);
	DefineKeyboard();
	
	SetConsoleTextColor(SystemColors::Red);
	SetCursorPosition();

	MoveWindow(_Hwnd, LayoutConstants::kLeftOffset, LayoutConstants::kTopOffset, LayoutConstants::kRightOffset - LayoutConstants::kLeftOffset, LayoutConstants::kBottomOffset - LayoutConstants::kTopOffset, TRUE);
}

void Console::SetConsoleTextColor(int kColorOffset)
{
	SetConsoleTextAttribute(_Console, kColorOffset | kColorOffset | FOREGROUND_INTENSITY);	
	
}

void Console::SetCursorPosition(int kXAxis, int kYAxis)
{
	COORD pos = { kXAxis, kYAxis };
	SetConsoleCursorPosition(_Console, pos);	
}

void Console::DefineKeyboard()
{
	SetConsoleTextColor(SystemColors::Blue);		
	
	int breakIndex = 0;
	int xAxis = KeyboardXPos[breakIndex];
	int yAxis = 1;
	int breakValue = KeyboardBreaks[breakIndex];
	for (int i = 0; i < sizeof(KeyboardLayout)/ sizeof(KeyboardLayout[0]) ; i++)
	{
		--breakValue;		
		if (isdigit(KeyboardLayout[i]))
		{
			keyboardIndexMap.insert(std::make_pair(KeyboardLayout[i] - 48, i));
			std::shared_ptr<Numeric> intKey = std::make_shared<Numeric>(KeyboardLayout[i] - 48, xAxis, yAxis);			
			KeyCollection.push_back(intKey);			
		}
		else
		{				
			keyboardIndexMap.insert(std::make_pair(KeyboardLayout[i], i));
			std::shared_ptr<Character> charKey = std::make_shared<Character>(KeyboardLayout[i], xAxis, yAxis);
			KeyCollection.push_back(charKey);
		}	

		if (breakValue)
		{
			xAxis += 3;
		}
		else
		{
			breakValue = KeyboardBreaks[++breakIndex];
			xAxis = KeyboardXPos[breakIndex];
			yAxis += 2;
		}
	}
}

void Console::CreateKeys()
{
	m_pKey = new Keys(KeyCollection);
	m_pKey->PrintKeys();
	SetCursorPosition();
}

void Console::SetFontsize(int xWidth, int yWidth)
{
	PCONSOLE_FONT_INFOEX lpConsoleCurrentFontEx = new CONSOLE_FONT_INFOEX();
	lpConsoleCurrentFontEx->cbSize = sizeof(CONSOLE_FONT_INFOEX);
	GetCurrentConsoleFontEx(_Console, 0, lpConsoleCurrentFontEx);
	lpConsoleCurrentFontEx->dwFontSize.X = xWidth;
	lpConsoleCurrentFontEx->dwFontSize.Y = yWidth;
	SetCurrentConsoleFontEx(_Console, 0, lpConsoleCurrentFontEx);
}

void Console::Animate(const std::string &inp)
{
	unordered_multimap<char, int> inpMap;
	map<char, int>::iterator inpMapIter;
	
	for (int i = 0; i < inp.size(); i++)
	{
		if (isdigit(inp[i]))
			inpMapIter = keyboardIndexMap.find(inp[i] - 48);
		else
			inpMapIter =  keyboardIndexMap.find(toupper( inp[i]));
		if (inpMapIter != keyboardIndexMap.end())
		{
			inpMap.insert(std::make_pair(inp[i], inpMapIter->second));
		}			
	}
	m_pKey->AnimateKeys(inpMap);
}

void Console::ProcessInput()
{	
	COORD pos = { m_OutputXAxisCoords, m_OutputYAxisCoords };
	SetConsoleCursorPosition(_Console, root);
	SetConsoleTextColor(SystemColors::Red);
	std::string wInpString;
	std::getline(cin, wInpString);
	if (wInpString == "ctrl q")
	{
		m_bConsoleRunning = false;
		SetConsoleCursorPosition(_Console, pos);
		std::cout << "\t \t \t \t EXITING VIRTUAL KEYBOARD ";
		Sleep(3000);
		return;
	}
	std::unique_ptr<InputString> inpPtr = std::make_unique<InputString>(wInpString);

	
	SetConsoleTextColor(SystemColors::Green);
	SetConsoleCursorPosition(_Console, pos);
	cout << inpPtr->GetResult();
	m_OutputYAxisCoords++;

	SetConsoleCursorPosition(_Console, root);
	for (int i = 0; i < wInpString.length(); i++)
	{
		cout << " ";
	}
	Animate(wInpString);

}



