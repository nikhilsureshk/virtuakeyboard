#ifndef GAME_CONSTANTS
#define GAME_CONSTANTS

namespace VirtualKeyboard
{
	namespace LayoutConstants
	{
		constexpr unsigned int kLeftOffset = 300U;
		constexpr unsigned int kTopOffset = 500U;
		constexpr unsigned int kRightOffset = 1800U;
		constexpr unsigned int kBottomOffset = 1000U;
	}	// namespace LayoutConstants

	enum SystemColors
	{
		Blue = 0x0001,
		Green = 0x0002,
		Red = 0x0004 
	};

	enum CharType
	{
		Int,
		Char
	};

	enum CursorPositions
	{
		InputXAxis = 0,
		InputYAxis = 0,
		OutputXAxis = 0,
		OutputYAxis = 8
	};

	static const char KeyboardLayout[] = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M' };
	static const int KeyboardBreaks[] = { 10, 10, 9, 7 };
	static const int KeyboardXPos[] = { 30, 30, 32, 35 };
	static constexpr char AllowedOperator = '+';
}

#endif // GAME_CONSTANTS

