#pragma once
#include <iostream>
#include "Literal.h"
#include "Console.h"

namespace VirtualKeyboard
{
	class Numeric :
		public Literal
	{
	public:
		Numeric() = delete;
		Numeric(const int &ref, const int& xAxisPos, const int& yAxisPos);
		virtual void SetKeyType(const int keyType);
		virtual int GetKey() const;
	private:
		int m_uIntData;
	};

}

