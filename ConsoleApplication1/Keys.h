#pragma once

#include <vector>
#include "Literal.h"
#include "Numeric.h"
#include "Character.h"
#include <unordered_map>

namespace VirtualKeyboard
{
	class Keys
	{
	public:
		Keys() = delete;	
		Keys(const	std::vector<std::shared_ptr<Literal>> &ref);
	
		void AnimateKeys(std::unordered_multimap< char, int> &inpMap);
		void PrintKeys();
	private:
		std::vector<std::shared_ptr<Literal>> m_vKeys;
		Console* _Console;
	};

}

