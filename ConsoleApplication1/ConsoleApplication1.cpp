#include <iostream>
#include <windows.h>
#include <conio.h>
#include "GameConstants.h"
#include "Console.h"
#include <thread>
#include <string>

using namespace std;
using namespace VirtualKeyboard;

// Driver Code


int main()
{	
	Console* _Console = Console::GetInstance();
	_Console->CreateKeys();	
	std::string inputString;
	while(_Console->GetConsoleRunning())
	{
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		_Console->ProcessInput();
	}

	return (0);
}
	
