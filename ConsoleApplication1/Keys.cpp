#include "Keys.h"


using namespace VirtualKeyboard;

Keys::Keys(const std::vector<std::shared_ptr<Literal>> &ref) :
	m_vKeys(ref)	
{
}

void Keys::PrintKeys()
{
	if (!_Console)
		_Console = Console::GetInstance();
	_Console->SetConsoleTextColor(SystemColors::Blue);
	std::vector<std::shared_ptr<Literal>>::iterator it;

	for (it = m_vKeys.begin(); it != m_vKeys.end(); it++)
	{
		int literalType = (*it)->GetKeyType();
		if (!literalType)
		{			
			std::shared_ptr<Numeric> numPtr =	std::dynamic_pointer_cast<Numeric> (*it);
			_Console->PrintOutput(numPtr->GetKey(), numPtr->GetXAxis(), numPtr->GetYAxis());
		}
		else
		{
			std::shared_ptr<Character> charPtr = std::dynamic_pointer_cast<Character> (*it);
			_Console->PrintOutput(charPtr->GetKey(), charPtr->GetXAxis(), charPtr->GetYAxis());
		}
		
		int y = 0;
	}
		
}

void Keys::AnimateKeys(std::unordered_multimap< char, int> &inpMap)
{
	std::unordered_multimap<char, int>::iterator inpMapIter;
	for (inpMapIter = inpMap.begin(); inpMapIter != inpMap.end(); inpMapIter++)
	{
		if (!m_vKeys[inpMapIter->second]->GetKeyType())
		{
			std::shared_ptr<Numeric> numPtr = std::dynamic_pointer_cast<Numeric>(m_vKeys[inpMapIter->second]);
			_Console->Blink(numPtr->GetKey(), numPtr->GetXAxis(), numPtr->GetYAxis());
		}
		else
		{
			std::shared_ptr<Character> charPtr = std::dynamic_pointer_cast<Character>(m_vKeys[inpMapIter->second]);
			_Console->Blink(charPtr->GetKey(), charPtr->GetXAxis(), charPtr->GetYAxis());
		}	
	}
	_Console->SetCursorPosition();
}